package main

import (
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

type BEFormatReport struct {
	Header string
	Body   []BEFormatReportEntry
	Footer string
}

type BEFormatReportEntry interface {
	ToFixedLengthString() string
}

type KPAYAUTOMAIL struct {
	RECTYPE      string
	MID          string
	REPORTCODE   string
	AUTOMAILFLAG string
}

func (e KPAYAUTOMAIL) ToFixedLengthString() string { //for body report
	return fmt.Sprintf(
		"%s%s%s%s",
		fmt.Sprintf("%1s", e.RECTYPE),
		fmt.Sprintf("%-9s", e.MID),
		fmt.Sprintf("%-3s", e.REPORTCODE),
		fmt.Sprintf("%1s", e.AUTOMAILFLAG),
	)
}

type KPAYAUTOMAIL2 struct {
	RECTYPE      string
	MID          string
	REPORTCODE   string
	AUTOMAILFLAG string
}

func (e KPAYAUTOMAIL2) ToFixedLengthString() string { //for body report
	return fmt.Sprintf(
		"%s%s%s%s",
		fmt.Sprintf("%-2s", e.RECTYPE),
		fmt.Sprintf("%-2s", e.MID),
		fmt.Sprintf("%-3s", e.REPORTCODE),
		fmt.Sprintf("%1s", e.AUTOMAILFLAG),
	)
}

func main() {
	y := time.Now()
	var report BEFormatReport

	report.Header = fmt.Sprintf(
		"%s%s%s%s%s%s%s%s",
		fmt.Sprintf("%2s", "00"),
		fmt.Sprintf("%-20s", "KB001_KBPP_AREXA"),
		fmt.Sprintf("%-2s", "BE"),
		fmt.Sprintf("%6s", "000009"),
		fmt.Sprintf("%8s", "YYYYMMDD"),
		fmt.Sprintf("%-5s", "733"),
		fmt.Sprintf("%-33s", "YYYY-MM-DDT hh:mm:ss[.s[s*]][TZD]"),
		fmt.Sprintf("%-824s", ""),
	)

	//
	//-- struct difference Lenght for generate BE file format
	//
	var am KPAYAUTOMAIL //struct first
	for i := 0; i < 10; i++ {
		am.RECTYPE = "1"
		am.MID = "MER12345"
		am.REPORTCODE = "12"
		am.AUTOMAILFLAG = "1"
		report.Body = append(report.Body, am)
	}

	var am2 KPAYAUTOMAIL2 //struct second
	for i := 0; i < 10; i++ {
		am2.RECTYPE = "1"
		am2.MID = "M"
		am2.REPORTCODE = "R"
		am2.AUTOMAILFLAG = "1"
		report.Body = append(report.Body, am2)
	}

	report.Footer = fmt.Sprintf(
		"%s%s%s%s%s%s%s%s%s",
		fmt.Sprintf("%2s", "00"),
		fmt.Sprintf("%-20s", "KB001_KBPP_AREXA"),
		fmt.Sprintf("%-2s", "BE"),
		fmt.Sprintf("%6s", "000009"),
		fmt.Sprintf("%8s", "YYYYMMDD"),
		fmt.Sprintf("%015d", len(report.Body)),
		fmt.Sprintf("%020s", "9"), //mock debit
		fmt.Sprintf("%020s", "9"), //mock credit
		fmt.Sprintf("%-807s", ""),
	)
	report.GenerateTXTFileForReport(&y)

}

func (e BEFormatReport) GenerateTXTFileForReport(d *time.Time) error {

	if err := os.MkdirAll(fmt.Sprintf("%s/%s", "./reportfile", d.Format("20060102")), os.ModePerm); err != nil {
		log.Errorln("No target DPX directiory:", d.Format("20060102"), err)
		return err
	}

	fileName := fmt.Sprintf("%s/%s/BE_0733_%s.txt", "./reportfile", d.Format("20060102"), d.Format("20060102"))
	log.Infoln("Generate File Report | ", fileName)

	f, err := os.Create(fileName)
	if err != nil {
		return err
	}
	defer f.Close()

	f.WriteString(e.Header)
	f.WriteString("\n")
	for _, b := range e.Body {
		f.WriteString(b.ToFixedLengthString())
		f.WriteString("\n")
	}
	f.WriteString(e.Footer)
	f.Sync()

	return nil
}
